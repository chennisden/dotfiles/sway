# This file defines and toggles workspaces

set $ws0 "0 [ANAL]"
set $ws1 "1 [ANAL]"
set $ws2 "2 [AG]"
set $ws3 "3 [AG]"
set $ws4 "4 [213]"
set $ws5 "5 [213]"
set $ws6 "6 [213]"
set $ws7 "7 [451]"
set $ws8 "8 [PUZZ]"
set $ws9 "9 [TA]"
set $ws10 "10 [RIT]"
set $ws11 "11 [MISC]"
set $ws12 "12 [MISC]"

bindsym $mod+grave workspace $ws0
bindsym $mod+1 workspace $ws1
bindsym $mod+2 workspace $ws2
bindsym $mod+3 workspace $ws3
bindsym $mod+4 workspace $ws4
bindsym $mod+5 workspace $ws5
bindsym $mod+6 workspace $ws6
bindsym $mod+7 workspace $ws7
bindsym $mod+8 workspace $ws8
bindsym $mod+9 workspace $ws9
bindsym $mod+0 workspace $ws10
bindsym $mod+minus workspace $ws11
bindsym $mod+equal workspace $ws12

bindsym $mod+Shift+grave move container to workspace $ws0
bindsym $mod+Shift+1 move container to workspace $ws1
bindsym $mod+Shift+2 move container to workspace $ws2
bindsym $mod+Shift+3 move container to workspace $ws3
bindsym $mod+Shift+4 move container to workspace $ws4
bindsym $mod+Shift+5 move container to workspace $ws5
bindsym $mod+Shift+6 move container to workspace $ws6
bindsym $mod+Shift+7 move container to workspace $ws7
bindsym $mod+Shift+8 move container to workspace $ws8
bindsym $mod+Shift+9 move container to workspace $ws9
bindsym $mod+Shift+0 move container to workspace $ws10
bindsym $mod+Shift+minus move container to workspace $ws11
bindsym $mod+Shift+equal move container to workspace $ws12

bindsym $mod+Shift+backspace move scratchpad
bindsym $mod+backspace scratchpad show
