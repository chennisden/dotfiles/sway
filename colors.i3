# literally just what colors i use for i3 window

set $bar-color #070707
set $bg-color #009F6B
set $inactive-bg-color #2f343f
set $text-color #f3f4f5
set $inactive-text-color #676E7D
set $urgent-bg-color #E53935

# window colors
#                       border              background         text                 indicator
client.focused          $bg-color           $bg-color          $text-color          #00ff00
client.unfocused        $inactive-bg-color $inactive-bg-color $inactive-text-color #00ff00
client.focused_inactive $inactive-bg-color $inactive-bg-color $inactive-text-color #00ff00
client.urgent           $urgent-bg-color    $urgent-bg-color   $text-color          #00ff00

hide_edge_borders both

# Start i3bar to display a workspace bar (plus the system information i3status
# finds out, if available)
bar {
	font pango:System San Francisco Display 13
	position top
	status_command i3blocks
	colors {
		background $bar-color
		separator #757575
		#	border	background	text
		focused_workspace	$bar-color	$bar-color	$text-color
		inactive_workspace	$bar-color	$bar-color	$inactive-text-color
		urgent_workspace	$urgent-bg-color	$urgent-bg-color	$text-color
	}
}
