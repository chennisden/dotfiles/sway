# Opening programs

bindsym $mod+Return exec alacritty --working-directory=$HOME
bindsym $mod+q kill

# dmenu
bindsym $mod+d exec bemenu-run

# grimshot (screenshots)
bindsym --release Print exec grimshot copy area
bindsym --release Shift+Print exec grimshot copy screen

# shutdown
bindsym $mod+Shift+s exec doas poweroff

# launcher mode
mode "launcher" {
	bindsym q exec qutebrowser; mode "default"
	bindsym f exec firefox; mode "default"
	# school profile of firefox
	bindsym s exec firefox -P school; mode "default"
	bindsym c exec chromium-browser; mode "default"
	bindsym b exec btd; mode "default"
	bindsym d exec discord; mode "default"
	bindsym z exec zathura; mode "default"
	bindsym Escape mode "default"
}

bindsym $mod+o mode "launcher"
