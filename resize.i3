# use $mod+r to resize windows

mode "resize" {
        # These bindings trigger as soon as you enter the resize mode

        # Pressing h will shrink the window’s width.
        # Pressing j will grow the window’s height.
        # Pressing k will shrink the window’s height.
        # Pressing l will grow the window’s width.
        bindsym h resize shrink width 10 px or 10 ppt
        bindsym j resize grow height 10 px or 10 ppt
        bindsym k resize shrink height 10 px or 10 ppt
        bindsym l resize grow width 10 px or 10 ppt

        # back to normal: Escape or $mod+r
        bindsym Escape mode "default"
        bindsym $mod+r mode "default"
}

bindsym $mod+r mode "resize"
