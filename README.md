Symlink the appropriate .specific file (laptop.specific or whatever) to specific.i3

## Dependencies
Unless otherwise specified, each item in this list refers to the name of an
Alpine Linux (apk) package.
Said names will probably be the same or very similar for any reasonable Linux
package manager.
Some of these packages require the `community/edge` repository to be enabled.

- sway (duh), and the appropriate Wayland dependencies as well as a libseat backend
	- See the [Alpine Sway page](https://wiki.alpinelinux.org/wiki/Sway) for all the dependencies & necessary usergroups.
- alacritty
- alsa-utils
    - Probably also need sof-firmware for audio to work, this requires a reboot to work
- bemenu
- brightnessctl
- i3blocks
- My [i3blocks scripts and config](https://gitlab.com/chennisden/dotfiles/i3blocks)
	- See the README for its own dependencies.
- grimshot (for screenshots)
